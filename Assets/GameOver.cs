﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	public Sprite p1, p2, p3, p4;
	SpriteRenderer winnerSprite;

	// Use this for initialization
	void Start () {
		foreach (SpriteRenderer renderer in GetComponentsInChildren<SpriteRenderer> ()) {
			if (renderer.sprite == null) {
				winnerSprite = renderer;
			}
		}

		winnerSprite.sprite = getWinnerSprite();
	}

	Sprite getWinnerSprite() {
		if (GameController.currentWinnter == 1) {
			return p1;
		}
		if (GameController.currentWinnter == 2) {
			return p2;
		}
		if (GameController.currentWinnter == 3) {
			return p3;
		}
		if (GameController.currentWinnter == 4) {
			return p4;
		}
		return p1;
	}
	
	// Update is called once per frame
	void Update () {
		if ( Input.GetKey(KeyCode.P) ) {
			Application.LoadLevel(0);
		}
		if ( Input.GetKey(KeyCode.Q) ) {
			Application.Quit();
		}
	}

}
