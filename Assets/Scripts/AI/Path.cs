﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path {

	List<Vector3> path;
	Direction direction;

	public Path() {
		path = new List<Vector3> ();
	}

	public Path(List<Vector3> path) {
		this.path = path;
	}

	public List<Vector3> getPositions() {
		return path;
	}

	public Direction getDirection() {
		return direction;
	}

	public void addPosition(Vector3 position, Direction direction) {
		path.Add(position);
		this.direction = direction;
	}

	public Vector3 getNextPosition(bool remove) {
		if (isEmpty()) {
			Debug.LogError("Trying to return value when there is nothing to return");
			return new Vector3(-1, -1, -1);
		}

		Vector3 nextPosition = path[0];
		if (remove) {
			int initialCount = path.Count;
			path.RemoveAt(0);
			if (path.Count == initialCount) {
				Debug.LogError("Path list not shrunk");
			}
		}
		return nextPosition;
	}

	public bool hasNext() {
		return path.Count > 1;
	}

	public bool isEmpty() {
		return path.Count <= 0;
	}

	public static Path emptyPath() {
		return new Path();
	}

}