﻿using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class Pathfinder {

	private const int MAX_STEPS = 3;

	public Pathfinder() {

	}

	public List<Path> getValidPathsFrom(Vector3 position) {
		return getValidPathsFrom(position, 0);
	}

	private List<Path> getValidPathsFrom(Vector3 position, int steps) {
		//int nrOfSteps = steps + 1;
		List<Path> validPaths = new List<Path>();

		Vector3 positionUp = getNextPosition(position, Direction.Up);
		if (PositionUtils.isNotEqual(position, positionUp)) {
			Path path = new Path();
			path.addPosition(positionUp, Direction.Up);
			validPaths.Add(path);
		}
		Vector3 positionDown = getNextPosition(position, Direction.Down);
		if (PositionUtils.isNotEqual(position, positionDown)) {
			Path path = new Path();
			path.addPosition(positionDown, Direction.Down);
			validPaths.Add(path);
		}
		Vector3 positionLeft = getNextPosition(position, Direction.Left);
		if (PositionUtils.isNotEqual(position, positionLeft)) {
			Path path = new Path();
			path.addPosition(positionLeft, Direction.Left);
			validPaths.Add(path);
		}
		Vector3 positionRight = getNextPosition(position, Direction.Right);
		if (PositionUtils.isNotEqual(position, positionRight)) {
			Path path = new Path();
			path.addPosition(positionRight, Direction.Right);
			validPaths.Add(path);
		}
		//Debug.Log("Number of valid paths: " + validPaths.Count + " from (" + position + ")");
		if (validPaths.Count == 0) {
			Debug.Log ("Debug from position: " + position);
			Debug.Log("Up " + positionUp + "::" + getNextPositionDebugText(position, Direction.Up));
			Debug.Log("Down " + positionDown + "::" +  getNextPositionDebugText(position, Direction.Down));
			Debug.Log("Left " + positionLeft + "::" + getNextPositionDebugText(position, Direction.Left));
			Debug.Log("Right " + positionRight + "::" + getNextPositionDebugText(position, Direction.Right));
			Time.timeScale = 0; 
		} 
		return validPaths;
	}

	private string getNextPositionDebugText(Vector3 root, Direction dir) {
		Tile position = LevelController.getClosestTileFor(LevelController.getPositionInFrontOf(root, dir));
		if (LevelController.fragileWallIsPlacedOn(position)) {
			return " FRAGILE WALL";
		}
		if (LevelController.solidWallIsPlacedOn(position)) {
			return " SOLID WALL";
		}
		return "no-wall";
	}

	private Vector3 getNextPosition(Vector3 root, Direction dir) {
		Tile position = LevelController.getClosestTileFor(LevelController.getPositionInFrontOf(root, dir));
		if (!LevelController.fragileWallIsPlacedOn(position) && !LevelController.solidWallIsPlacedOn(position)) {
			return position.position;
		}
		return root;
	}

	public bool isDangerousPosition(Vector3 position) {
		Bomb[] bombs = GameController.getActiveBombs();
		float tileSize = LevelController.getTileSize();
		float dangerZone = (tileSize * 3) + (tileSize / 4);

		foreach (Bomb bomb in bombs) {
			if (isWithinExplosionRange(position, bomb.transform.position, dangerZone)) {
				return true;
			}
		}
		return false;
	}

	private bool isWithinExplosionRange(Vector3 position, Vector3 bombPosition, float dangerZone) {
		if (!PositionUtils.isOnSameLane(position, bombPosition)) return false;

		float distanceToBomb = PositionUtils.getDistanceBetween(position, bombPosition);
		return distanceToBomb < dangerZone;
	}	

	public bool canPlaceBombAndMoveAway(Vector3 position) {
		//return false;
		//return isSafeTileTwoMovesAway(position);
		//return isSafeTileTwoMovesAway(position) || canMoveFourStraightTiles(position);
		return isSafeTileFourMovesAwayFrom(position);
	}

	private bool isSafeTileFourMovesAwayFrom(Vector3 bombPosition) {
		List<Vector3> positions = getReachablePositionsFrom(bombPosition, 4);
		Debug.Log("Positions: " + positions.Count);
		//Debug.Break();

		if (positions.Count > 0) {
			float tileSize = LevelController.getTileSize();
			float dangerZone = (tileSize * 3) + (tileSize / 4);

			foreach (Vector3 position in positions) {
				if (!isWithinExplosionRange(position, bombPosition, dangerZone)) {
					return true;
				}
			}
		}
		return false;
	}

	// How far can you get from 'root' if you can go 'steps' amount of steps
	private List<Vector3> getReachablePositionsFrom (Vector3 root, int steps) {
		List<Vector3> positions = new List<Vector3> ();
		int stepsLeft = steps - 1;

		Vector3 positionAbove = getPositionNextTo(root, Direction.Up);
		if (isValid(positionAbove)) {
			positions.Add(positionAbove);
			if (steps > 0) {
				positions.AddRange(getReachablePositionsFrom(positionAbove, stepsLeft));
			}
		}
		Vector3 positionBelow = getPositionNextTo(root, Direction.Down);
		if (isValid(positionBelow)) {
			positions.Add(positionBelow);
			if (steps > 0) {
				positions.AddRange(getReachablePositionsFrom(positionBelow, stepsLeft));
			}
		}
		Vector3 positionRight = getPositionNextTo(root, Direction.Right);
		if (isValid(positionRight)) {
			positions.Add(positionRight);
			if (steps > 0) {
				positions.AddRange(getReachablePositionsFrom(positionRight, stepsLeft));
			}
		}
		Vector3 positionLefty = getPositionNextTo(root, Direction.Left);
		if (isValid(positionLefty)) {
			positions.Add(positionLefty);
			if (steps > 0) {
				positions.AddRange(getReachablePositionsFrom(positionLefty, stepsLeft));
			}
		}

		return positions;
	}

	private Vector3 getPositionNextTo(Vector3 root, Direction direction) {
		bool invalid = false;
		Vector3 position = LevelController.getPositionInFrontOf(root, direction);
		invalid |= LevelController.isOutOfBounds(position);
		invalid |= LevelController.fragileWallIsPlacedOn(position);
		invalid |= LevelController.solidWallIsPlacedOn(position);

		if (invalid) {
			return invalidate(position);
		}
		return position;
	}

	private Vector3 invalidate(Vector3 position) {
		return new Vector3(position.x, position.y, Z.invalid);
	}

	private bool isValid(Vector3 position) {
		return position.z != Z.invalid;
	}

	//FIXME will never be true
	private bool isSafeTileTwoMovesAway(Vector3 position) {
		Vector3 neighbor;
		neighbor = LevelController.getPositionInFrontOf(position, Direction.Up);		
		if (!LevelController.isOutOfBounds(neighbor)) {
			if (neighboringTileIsSafe(neighbor)) {
				return true;
			}
		}
		neighbor = LevelController.getPositionInFrontOf(position, Direction.Down);
		if (!LevelController.isOutOfBounds(neighbor)) {
			if (neighboringTileIsSafe(neighbor)) {
				return true;
			}
		}
		neighbor = LevelController.getPositionInFrontOf(position, Direction.Left);
		if (!LevelController.isOutOfBounds(neighbor)) {
			if (neighboringTileIsSafe(neighbor)) {
				return true;
			}
		}
		neighbor = LevelController.getPositionInFrontOf(position, Direction.Right);
		if (!LevelController.isOutOfBounds(neighbor)) {
			if (neighboringTileIsSafe(neighbor)) {
				return true;
			}
		}
		return false;
	}

	private bool neighboringTileIsSafe(Vector3 position) {
		float tileSize = LevelController.getTileSize();
		float dangerZone = (tileSize * 3) + (tileSize / 4);
		Vector3 checkPosition;

		checkPosition = LevelController.getPositionInFrontOf(position, Direction.Up);
		if (!LevelController.isOutOfBounds(checkPosition)) {
			if (!isWithinExplosionRange(position, checkPosition, dangerZone)) {
				return true;
			}
		} else Debug.Log("IS OUT OF BOUNDS");
		checkPosition = LevelController.getPositionInFrontOf(position, Direction.Down);
		if (!LevelController.isOutOfBounds(checkPosition)) {
			if (!isWithinExplosionRange(position, checkPosition, dangerZone)) {
				return true;
			}		}
		checkPosition = LevelController.getPositionInFrontOf(position, Direction.Left);
		if (!LevelController.isOutOfBounds(checkPosition)) {
			if (!isWithinExplosionRange(position, checkPosition, dangerZone)) {
				return true;
			}		}
		checkPosition = LevelController.getPositionInFrontOf(position, Direction.Right);
		if (!LevelController.isOutOfBounds(checkPosition)) {
			if (!isWithinExplosionRange(position, checkPosition, dangerZone)) {
				return true;
			}		
		}
		return false;
	}

	private bool canMoveFourStraightTiles(Vector3 position) {

		return false;
	}

}
