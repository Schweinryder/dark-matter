﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

	public AudioClip explosionSound;

	private const float BOMB_TIMER = 3f;
	float blinkRate = .2f;
	float blinkTimer;

	float timer;
	bool exploded;
	BombSpriteSet spriteSet;

	int playerNr;
	int strength;

	SpriteRenderer spriteRenderer;
	Sprite bomb, bombBlink;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponentInChildren<SpriteRenderer> ();
		bomb = spriteRenderer.sprite;

		timer = BOMB_TIMER;
		exploded = false;
	}

	public void init(int playerNr, int strength) {
		this.playerNr = playerNr;
		this.strength = strength;
		initSpriteSet();
		initBlinkColor();
		GameController.addActiveBombForPlayer(playerNr);
	}

	void initBlinkColor() {
		if (playerNr == 1) bombBlink = GameController.bombGreenBlink;
		else if (playerNr == 2) bombBlink = GameController.bombRedBlink;
		else if (playerNr == 3) bombBlink = GameController.bombYellowBlink;
		else if (playerNr == 4) bombBlink = GameController.bombBlueBlink;
		else {
			bombBlink = GameController.bombGreenBlink;
		}
	}

	void initSpriteSet() {
		if (playerNr == 1) spriteSet = GameController.greenExplSet;
		else if (playerNr == 2) spriteSet = GameController.redExplSet;
		else if (playerNr == 3) spriteSet = GameController.blueExplSet;
		else if (playerNr == 4) spriteSet = GameController.yellowExplSet;
		else {
			spriteSet = GameController.greenExplSet;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!exploded) {
			checkBlink();

			timer -=Time.deltaTime;
			if (timer <= 0) {
				explode();
			}
		}
	}

	void checkBlink() {
		blinkTimer += Time.deltaTime;
		if (blinkTimer >= blinkRate) {
			blink ();
			blinkTimer = 0;
		}
		if (timer < 1f) {
			blinkRate = .1f;
		}
	}

	void blink() {
		if (spriteRenderer.sprite == bomb) {
			spriteRenderer.sprite = bombBlink;
		} else {
			spriteRenderer.sprite = bomb;
		}
	}

	public void explode() {
		exploded = true;
		playSound();
		GameController.removeActiveBombForPlayer(playerNr);
		renderExplosion();
		Destroy(this.gameObject);
	}

	void playSound() {
		if (explosionSound != null) {
			SoundController.playSound(explosionSound);
		}
	}

	private void renderExplosion() {
		Instantiate (spriteSet.explStart, new Vector3(transform.position.x, transform.position.y, Z.explosion), Quaternion.identity);
		explode(Direction.Up);
		explode(Direction.Right);
		explode(Direction.Down);
		explode(Direction.Left);
	}

	private void explode(Direction direction) {
		Vector3 currentPosition = transform.position;
		int powerLeft = strength;
		while (powerLeft > 0) {
			currentPosition = LevelController.getPositionInFrontOf(currentPosition, direction);
			if (LevelController.isOutOfBounds(currentPosition)) {
				return;
			}
			Tile tile = LevelController.getClosestTileFor(currentPosition);
			if (LevelController.solidWallIsPlacedOn(tile)) {
				return;
			}
			if (LevelController.fragileWallIsPlacedOn(tile)) {
				powerLeft = 1;
			}

			GameObject explosionObject;
			Vector3 explosionPosition = new Vector3(tile.position.x, tile.position.y, Z.explosion);
			if (powerLeft == 1) {
				explosionObject = Instantiate (spriteSet.explEnd, explosionPosition, Quaternion.identity) as GameObject;
			} else {
				explosionObject = Instantiate (spriteSet.explMid, explosionPosition, Quaternion.identity) as GameObject;
			}

			if (explosionObject != null) {
				explosionObject.GetComponent<SpriteObject> ().setDirection(direction);
			}
			powerLeft--;
		}
	}
}
