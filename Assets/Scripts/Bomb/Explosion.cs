﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	private const float EXPLOSION_TIME = .8f;

	float timer;

	// Use this for initialization
	void Start () {
		timer = EXPLOSION_TIME;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			Destroy(this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Character character = other.gameObject.GetComponent<Character> ();
		if (character != null) {
			character.applyDamage();
			return;
		}
		Bomb bomb = other.gameObject.GetComponent<Bomb> ();
		if (bomb != null) {
			bomb.explode();
			return;
		}
		FragileWall wall = other.gameObject.GetComponent<FragileWall> ();
		if (wall != null) {
			LevelController.removeWall(wall);
			return;
		}
	}

}
