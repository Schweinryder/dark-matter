﻿using UnityEngine;
using System.Collections;

public class BombSpriteSet {

	public GameObject explStart {get; private set;} 
	public GameObject explMid {get; private set;} 
	public GameObject explEnd {get; private set;}

	public BombSpriteSet(GameObject start, GameObject mid, GameObject end) {
		this.explStart = start;
		this.explMid = mid;
		this.explEnd = end;
	}
	
}
