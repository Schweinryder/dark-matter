﻿using UnityEngine;
using System.Collections;

public class GameAssetLoader : MonoBehaviour {

	public GameObject level1Prefab;
	public GameObject getLevel1() {
		return level1Prefab;
	}

	public GameObject playerPrefab;
	public GameObject getPlayerPrefab() {
		return playerPrefab;
	}

	public GameObject enemyPrefab;
	public GameObject getEnemyPrefab() {
		return enemyPrefab;
	}

	public Sprite player1Sprite;
	public Sprite getPlayer1Sprite() {
		return player1Sprite;
	}

	public Sprite player2Sprite;
	public Sprite getPlayer2Sprite() {
		return player2Sprite;
	}

	public Sprite player3Sprite;
	public Sprite getPlayer3Sprite() {
		return player3Sprite;
	}

	public Sprite player4Sprite;
	public Sprite getPlayer4Sprite() {
		return player4Sprite;
	}

	public GameObject powerUpPrefab;
	public GameObject getPowerUpPrefab() {
		return powerUpPrefab;
	}
	
	public GameObject greenExplStart;
	public GameObject greenExplMid;
	public GameObject greenExplEnd;
	public BombSpriteSet getGreenBombSpriteSet() {
		return new BombSpriteSet(greenExplStart, greenExplMid, greenExplEnd);
	}

	public GameObject redExplStart;
	public GameObject redExplMid;
	public GameObject redExplEnd;
	public BombSpriteSet getRedBombSpriteSet() {
		return new BombSpriteSet(redExplStart, redExplMid, redExplEnd);
	}

	public GameObject blueExplStart;
	public GameObject blueExplMid;
	public GameObject blueExplEnd;
	public BombSpriteSet getBlueBombSpriteSet() {
		return new BombSpriteSet(blueExplStart, blueExplMid, blueExplEnd);
	}

	public GameObject yellowExplStart;
	public GameObject yellowExplMid;
	public GameObject yellowExplEnd;
	public BombSpriteSet getYellowBombSpriteSet() {
		return new BombSpriteSet(yellowExplStart, yellowExplMid, yellowExplEnd);
	}

	public Sprite greenBlinkSprite;
	public Sprite getGreenBlinkSprite() {
		return greenBlinkSprite;
	}

	public Sprite redBlinkSprite;
	public Sprite getRedBlinkSprite() {
		return redBlinkSprite;
	}

	public Sprite yellowBlinkSprite;
	public Sprite getYellowBlinkSprite() {
		return yellowBlinkSprite;
	}

	public Sprite blueBlinkSprite;
	public Sprite getBlueBlinkSprite() {
		return blueBlinkSprite;
	}


}
