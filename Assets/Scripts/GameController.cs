﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public float gameSpeed = 1; // 1

	// Score
	public static int currentWinnter;
	public static int Player1Wins;
	public static int Player2Wins;
	public static int Player3Wins;
	public static int Player4Wins;

	// Game variables
	public static int player1ActiveBombs;
	public static int player2ActiveBombs;
	public static int player3ActiveBombs;
	public static int player4ActiveBombs;

	// Game constants
	public const int MaxPlayers = 4;
	public const float MaxSpeed = 4f;
	public const int MaxBombPower = 5;
	public const int MaxNumberOfBombs = 6;
	public const int MaxNumberOfWalls = 3;

	// Static objects needed by other objects
	public static GameObject powerUpObject {get; private set;}
	public static BombSpriteSet greenExplSet {get; private set;}
	public static BombSpriteSet blueExplSet {get; private set;}
	public static BombSpriteSet yellowExplSet {get; private set;}
	public static BombSpriteSet redExplSet {get; private set;}

	public static Sprite bombGreenBlink {get; private set;}
	public static Sprite bombRedBlink {get; private set;}
	public static Sprite bombYellowBlink {get; private set;}
	public static Sprite bombBlueBlink {get; private set;}

	// Objects only needed within GameController
	GameAssetLoader assets;
	LevelController levelController;
	GameObject level1prefab;
	GameObject levelInstance;
	public GameObject gameOver;
	public GameObject gameInit;

	bool gameLoaded;


	// Player | Enemy
	public static bool enemyActive {get; private set;}
	public static List<Character> currentPlayers {get; private set;}

	void Awake() {
		loadAssets();
	}

	void loadAssets() {
		assets = GetComponent<GameAssetLoader> ();

		level1prefab 	= assets.getLevel1();

		powerUpObject 	= assets.getPowerUpPrefab();
		greenExplSet 	= assets.getGreenBombSpriteSet();
		redExplSet		= assets.getRedBombSpriteSet();
		blueExplSet 	= assets.getBlueBombSpriteSet();
		yellowExplSet 	= assets.getYellowBombSpriteSet();

		bombGreenBlink 	= assets.getGreenBlinkSprite();
		bombRedBlink 	= assets.getRedBlinkSprite();
		bombYellowBlink = assets.getYellowBlinkSprite();
		bombBlueBlink 	= assets.getBlueBlinkSprite();
	}

	// Use this for initialization
	void Start () {
		levelController = FindObjectOfType<LevelController> ();
		gameLoaded = false;
		Instantiate (gameInit, gameInit.transform.position, Quaternion.identity);
		Time.timeScale = gameSpeed;
	}

	public void reloadGame() {
		removeEverything();
		loadGame();
	}

	public void loadGame() {
		if (!gameLoaded) {
			initLevel();
			levelController.reloadLevel();
			initPlayers();
			gameLoaded = true;
		}
	}

	void initLevel() {
		levelInstance = Instantiate(level1prefab, level1prefab.transform.position, Quaternion.identity) as GameObject;
	}

	void removeEverything() {
		levelController.resetLevel();
		Destroy(levelInstance);
		gameLoaded = false;
	}

	static float timer;
	void Update() {
		if (gameIsOver) {
			timer -= Time.deltaTime;
			if (timer <= 0) {
				Instantiate (gameOver, new Vector3(0,0,-8), Quaternion.identity);
				gameIsOver = false;
			}
		}
	}

	void initPlayers() {
		currentPlayers = new List<Character> ();
		for (int i = 1; i <= GameSettings.totalNumberOfPlayers; i++) {
			Character player;
			if (i <= GameSettings.numberOfPlayers) {
				player = spawnPlayer(assets.getPlayerPrefab(), i);
			} else {
				player = spawnPlayer(assets.getEnemyPrefab(), i);
			}
			currentPlayers.Add(player);
		}
	}

	static bool gameIsOver = false;
	public static void iWasKilled(int playerNr) {
		Character killedPlayer = null;
		foreach (Character player in currentPlayers) {
			if (playerNr == player.playerNumber) {
				killedPlayer = player;
				break;
			}
		}
		if (killedPlayer != null) {
			currentPlayers.Remove(killedPlayer);
		}
		if (currentPlayers.Count == 1) {
			gameIsOver = true;
			Character winner = currentPlayers[0];
			currentWinnter = winner.playerNumber;
			if (currentWinnter == 1) {
				Player1Wins++;
			} else if (currentWinnter == 2) {
				Player2Wins++;
			} else if (currentWinnter == 3) {
				Player3Wins++;
			} else if (currentWinnter == 4) {
				Player4Wins++;
			}
		}
	}

	Character spawnPlayer(GameObject playerToSpawn, int playerNr) {
		try {
			GameObject spawnedObj = Instantiate(playerToSpawn, getPositionFor(playerNr), Quaternion.identity) as GameObject;
			Character player = spawnedObj.GetComponent<Character> ();
			player.Init(playerNr);
			player.setSprite(getSpriteForPlayer(playerNr));
			if (player.GetType() == typeof(Player)) {
				player.gameObject.GetComponent<Player> ().initInput(getInputLayoutForPlayer(playerNr));
			}
			return player;
		}
		catch (Exception e) {
			Debug.LogError("Could not spawn player " + playerNr);
			throw new Exception(e.Message);
		}
	}

	Vector3 getPositionFor(int playerNr) {
		Tile startTile = getStartTileForPlayer(playerNr);
		return new Vector3(startTile.position.x, startTile.position.y, Z.player);
	}

	Tile getStartTileForPlayer(int playerNr) {
		if (playerNr == 1) {
			return LevelController.topLeftTile();
		} else if (playerNr == 2) {
			return LevelController.bottomRightTile();
		} else if (playerNr == 3) {
			return LevelController.bottomLeftTile();
		} else if (playerNr == 4) {
			return LevelController.topRightTile();
		} else {
			Debug.LogError("Player " + playerNr + " should not exist");
			return null;
		}
	}

	Sprite getSpriteForPlayer(int playerNr) {
		if (playerNr == 1) {
			return assets.getPlayer1Sprite();
		} else if (playerNr == 2) {
			return assets.getPlayer2Sprite();
		} else if (playerNr == 3) {
			return assets.getPlayer3Sprite();
		} else if (playerNr == 4) {
			return assets.getPlayer4Sprite();
		} else {
			Debug.LogError("Player " + playerNr + " should not exist");
			return null;
		}
	}

	InputLayout getInputLayoutForPlayer(int playerNr) {
		if (playerNr == 1) {
			return InputLayout.getDefaultPlayer1Layout();
		}
		if (playerNr == 2) {
			return InputLayout.getDefaultPlayer2Layout();
		}
		return null;
	}

	public static void addActiveBombForPlayer(int playerNr) {
		if (playerNr == 1) {
			player1ActiveBombs++;
		}
		if (playerNr == 2) {
			player2ActiveBombs++;
		}
		if (playerNr == 3) {
			player3ActiveBombs++;
		}
		if (playerNr == 4) {
			player4ActiveBombs++;
		}
	}

	public static void removeActiveBombForPlayer(int playerNr) {
		if (playerNr == 1) {
			player1ActiveBombs--;
		}
		if (playerNr == 2) {
			player2ActiveBombs--;
		}
		if (playerNr == 3) {
			player3ActiveBombs--;
		}
		if (playerNr == 4) {
			player4ActiveBombs--;
		}
	}

	public static int getNumberOfActiveBombForPlayer(int playerNr) {
		if (playerNr == 1) {
			return player1ActiveBombs;
		}
		if (playerNr == 2) {
			return player2ActiveBombs;
		}
		if (playerNr == 3) {
			return player3ActiveBombs;
		}
		if (playerNr == 4) {
			return player4ActiveBombs;
		}
		return -1;
	}

	public static Bomb[] getActiveBombs() {
		return FindObjectsOfType<Bomb> ();
	}
	
	
}
