﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]
public class GameInitializer : MonoBehaviour {

	GameController game;

	public Sprite start, controls;
	SpriteRenderer spriteRnderer;
	int shownSprites;

	// Use this for initialization
	void Start () {
		game = FindObjectOfType<GameController> ();

		spriteRnderer = GetComponent<SpriteRenderer> ();
		spriteRnderer.sprite = start;
		shownSprites = 1;	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return)) {
			if (shownSprites == 1) {
				spriteRnderer.sprite = controls;
				shownSprites++;
			} else {
				game.loadGame();
				Destroy(this.gameObject);
			}
		}
	}
}
