﻿using UnityEngine;
using System.Collections;

public class Ghost : Movable {

	float vanishCountdown;
	float speed;

	// Use this for initialization
	void Start () {
		checkBorder = checkLane = setDirection = false;
		vanishCountdown = 2f;
		speed = .6f;

		sprite = GetComponentInChildren<SpriteObject> ();
		sprite.startFadeout(2f);
	}
	
	void FixedUpdate () {
		transform.position = new Vector3(transform.position.x, transform.position.y + speed * Time.deltaTime, transform.position.z);
	}

	void Update() {
		vanishCountdown -= Time.deltaTime;
		if (vanishCountdown <= 0) {
			Destroy(this.gameObject);
		}
	}
}
