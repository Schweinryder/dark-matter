﻿using UnityEngine;
using System.Collections;

public class InputLayout {

	// Movement keys
	public KeyCode Up {get; private set;}
	public KeyCode Right {get; private set;}
	public KeyCode Down {get; private set;}
	public KeyCode Left {get; private set;}

	// Action keys
	public KeyCode bombAction {get; private set;}
	public KeyCode wallAction {get; private set;}

	public void setMovement(KeyCode up, KeyCode right, KeyCode down, KeyCode left) {
		this.Up = up;
		this.Right = right;
		this.Down = down;
		this.Left = left;
	}

	public void setAction(KeyCode bomb, KeyCode wall) {
		this.bombAction = bomb;
		this.wallAction = wall;
	}

	public static InputLayout getDefaultPlayer1Layout() {
		InputLayout layout = new InputLayout();
		layout.setMovement(KeyCode.W, KeyCode.D, KeyCode.S, KeyCode.A);
		layout.setAction(KeyCode.Space, KeyCode.N);
		return layout;
	}

	public static InputLayout getDefaultPlayer2Layout() {
		InputLayout layout = new InputLayout();
		layout.setMovement(KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.LeftArrow);
		layout.setAction(KeyCode.Return, KeyCode.Keypad2);
		return layout;
	}

}