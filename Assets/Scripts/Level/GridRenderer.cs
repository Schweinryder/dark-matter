﻿using UnityEngine;
using System.Collections;

public class GridRenderer : MonoBehaviour {

	public int size_x, size_z;
	public GameObject linePrefab;
	GameObject lineObject;
	LineRenderer currentLine;

	// Use this for initialization
	void Start () {
		//renderGrid();
	}

	private void renderGrid() {
		for (int x = 0; x < size_x; x++) {
			lineObject = Instantiate (linePrefab, new Vector3(0,1,0), Quaternion.identity) as GameObject;
			currentLine = lineObject.GetComponent<LineRenderer>();
			currentLine.SetPosition(0, new Vector3(0,1,x));
			currentLine.SetPosition(1, new Vector3(size_x,1,x));
		}
		
		for (int z = 0; z < size_z; z++){
			lineObject = Instantiate (linePrefab, new Vector3(0,1,0), Quaternion.identity) as GameObject;
			currentLine = lineObject.GetComponent<LineRenderer>();
			currentLine.SetPosition(0, new Vector3(z,1,0));
			currentLine.SetPosition(1, new Vector3(z,1,size_z));
		}
	}
}
