﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {

	public const float TOP_BORDER = 2.7f;
	public const float RIGHT_BORDER = 2.7f;
	public const float BOTTOM_BORDER = -2.7f;
	public const float LEFT_BORDER = -2.7f;

	public const int LEVEL_SIZE_X = 10;
	public const int LEVEL_SIZE_Y = 10;

	public bool doRenderHelpTiles;
	public Transform tilePrefab;

	static TileMap tileMap;
	static PixelGrid pixelGrid;

	public GameObject solidWallPrefab;
	public GameObject fragileWallPrefab;
	static GameObject fragileWall;

	static List<Wall> solidWalls;
	static List<Wall> fragileWalls;

	void Awake() {
		fragileWall = fragileWallPrefab;

		tileMap = new TileMap();
		tileMap.Init();

		pixelGrid = new PixelGrid();
	}

	void Start() {
		//loadLevel();
	}

	void loadLevel() {
		if (doRenderHelpTiles) {
			renderHelpTiles();
		}
		fragileWalls = new List<Wall> ();
		solidWalls = new List<Wall> ();
		foreach (Wall wall in FindObjectsOfType<Wall> ()) {
			if (wall.GetType() == typeof(SolidWall)) {
				solidWalls.Add(wall);
			}
		}
		placeFragileWalls();
	}

	public void resetLevel() {
		foreach (Wall wall in fragileWalls) {
			Destroy(wall.gameObject);
		}
		foreach (Wall wall in solidWalls) {
			Destroy(wall.gameObject);
		}
	}

	public void reloadLevel() {		
		loadLevel();
	}

	void placeFragileWalls() {
		Tile[,] map = tileMap.getTileMap();
		for (int x = 0; x < LEVEL_SIZE_X; x++) {
			for (int y = 0; y < LEVEL_SIZE_Y; y++) {
				Tile tile = map[x, y];
				if (canPlaceWallOnTile(tile)) {
					int value = Random.Range(0, 100);
					if (value > 20) {
						placeWallAt(tile.position);
					}
				}
			}
		}
	}

	bool canPlaceWallOnTile(Tile tile) {
		if (tile.positionX <= 1 && tile.positionY <= 1) {
			return false;
		}
		if (tile.positionX <= 1 && tile.positionY >= LEVEL_SIZE_Y - 2) {
			return false;
		}
		if (tile.positionX >= LEVEL_SIZE_X - 2 && tile.positionY <= 1) {
			return false;
		}
		if (tile.positionX >= LEVEL_SIZE_X - 2 && tile.positionY >= LEVEL_SIZE_Y - 2) {
			return false;
		}
		if (solidWallIsPlacedOn(tile)) {
			return false;
		}
		return true;
	}

	public static float gameAreaWidth() {
		return (LEFT_BORDER * -1) + RIGHT_BORDER;
	}

	public static float gameAreaHeight() {
		return (BOTTOM_BORDER * -1) + TOP_BORDER;
	}

	public static float getTileSize() {
		return gameAreaWidth() / (LEVEL_SIZE_X -1);
	}

	public static Tile getClosestTileFor(Vector3 position) {
		return tileMap.findClosestTileFor(position);
	}

	public static Tile getClosestTileFor(GameObject obj) {
		return tileMap.findClosestTileFor(obj.transform.position);
	}

	public static void placeWallAt(Vector3 position) {
		GameObject wallObject = Instantiate (fragileWall, new Vector3(position.x, position.y, Z.wall), Quaternion.identity) as GameObject;
		if (wallObject != null) {
			fragileWalls.Add(wallObject.GetComponent<FragileWall> ());
		}
	}

	public static bool placeWallAheadOf(Vector3 position, Direction direction) {
		Vector3 wallPosition = getPositionInFrontOf(position, direction);
		Tile tile = tileMap.findClosestTileFor(wallPosition);

		if (fragileWallIsPlacedOn(tile)) return false;

		GameObject wallObject = Instantiate (fragileWall, tile.position, Quaternion.identity) as GameObject;
		if (wallObject == null) {
			return false;
		}
		fragileWalls.Add(wallObject.GetComponent<FragileWall> ());
		return true;
	}

	public static bool pickupWallAheadOf(Vector3 position, Direction direction) {
		Vector3 wallPosition = getPositionInFrontOf(position, direction);
		Tile tile = tileMap.findClosestTileFor(wallPosition);
		Wall wall = getFragileWallOn(tile);
		if (wall == null) {
			return false;
		}
		if (wall.GetType() == typeof(FragileWall)) {
			removeWall((FragileWall) wall);
		} else return false;
		return true;
	}

	public static void removeWall(FragileWall wall) {
		fragileWalls.Remove(wall);
		wall.destroyWall();
	}

	public static float getPixelPositionX(float originalPositionX) {
		return pixelGrid.getValidPositionX(originalPositionX);
	}

	public static float getPixelPositionY(float originalPositionY) {
		return pixelGrid.getValidPositionY(originalPositionY);
	}

	public static bool isOutOfBounds(Vector3 position) {
		// Add small value to calculation, player can be positioned at like 1.032
		if (position.y > TOP_BORDER + .1f){ 
			return true;
		}
		if (position.y < BOTTOM_BORDER - .1f) { 
			return true;
		}
		if (position.x > RIGHT_BORDER + .1f) {
			return true; 
		}
		if (position.x < LEFT_BORDER - .1f) {
			return true;
		}
		return false;
	}

	public static Vector3 getPositionInFrontOf(Vector3 position, Direction direction) {
		if (direction == Direction.Up) {
			return new Vector3(position.x, position.y + getTileSize(), -1);
		} else if (direction == Direction.Right) {
			return new Vector3(position.x + getTileSize(), position.y, -1);
		} else 	if (direction == Direction.Down) {
			return new Vector3(position.x, position.y - getTileSize(), -1);
		} else  /* (direction == Direction.Left) */ {
			return new Vector3(position.x - getTileSize(), position.y, -1);
		}
	}

	public static bool solidWallIsPlacedOn(Vector3 position) {
		return solidWallIsPlacedOn(getClosestTileFor(position));
	}

	public static bool solidWallIsPlacedOn(Tile tile) {
		Tile compareTile;
		for (int i = 0; i < solidWalls.Count; i++) {
			compareTile = getClosestTileFor(solidWalls[i].transform.position);
			if (tile.position.x == compareTile.position.x && tile.position.y == compareTile.position.y) {
				return true;
			}
		}
		return false;
	}

	public static bool fragileWallIsPlacedOn(Vector3 position) {
		return fragileWallIsPlacedOn(getClosestTileFor(position));
	}

	public static bool fragileWallIsPlacedOn(Tile tile) {
		Tile compareTile;
		for (int i = 0; i < fragileWalls.Count; i++) {
			compareTile = getClosestTileFor(fragileWalls[i].transform.position);
			if (tile.position.x == compareTile.position.x && tile.position.y == compareTile.position.y) {
				return true;
			}
		}
		return false;
	}

	public static Wall getFragileWallOn(Tile tile) {
		Tile compareTile;
		for (int i = 0; i < fragileWalls.Count; i++) {
			Wall wall = fragileWalls[i];
			compareTile = getClosestTileFor(wall.transform.position);
			if (tile.position.x == compareTile.position.x && tile.position.y == compareTile.position.y) {
				return wall;
			}
		}
		return null;
	}

	public static float getDistanceBetweem(Vector3 position1, Vector3 position2) {
		return tileMap.getDistanceBetween(position1, position2);
	}

	public static bool isEqual(Vector3 position1, Vector3 position2) {
		return PositionUtils.isEqual(position1, position2);
	}

	public static Tile topLeftTile() {
		return getClosestTileFor(new Vector3(LEFT_BORDER, TOP_BORDER, 0));
	}

	public static Tile topRightTile() {
		return getClosestTileFor(new Vector3(RIGHT_BORDER, TOP_BORDER, 0));
	}

	public static Tile bottomLeftTile() {
		return getClosestTileFor(new Vector3(LEFT_BORDER, BOTTOM_BORDER, 0));
	}

	public static Tile bottomRightTile() {
		return getClosestTileFor(new Vector3(RIGHT_BORDER, BOTTOM_BORDER, 0));
	}

	void renderHelpTiles() {
		Tile[,] map = tileMap.getTileMap();
		for (int x = 0; x < LEVEL_SIZE_X; x++) {
			for (int y = 0; y < LEVEL_SIZE_Y; y++) {
				if (isEven(x) && !isEven(y)) {
					continue;
				} else if (!isEven(x) && isEven(y)) {
					continue;
				}
				Tile tile = map[x,y];
				Instantiate (tilePrefab, tile.position, Quaternion.identity);
			}
		}
	}

	bool isEven(int number) {
		return number % 2 != 0;
	}
}
