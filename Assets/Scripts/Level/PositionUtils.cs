﻿using UnityEngine;
using System;
using System.Collections;

public static class PositionUtils {

	public static bool isEqual(Vector3 position1, Vector3 position2) {		
		if (position1 == position2) {
			return true;
		}

		float posX1 = (float)Math.Round(position1.x, 1);
		float posX2 = (float)Math.Round(position2.x, 1);
		float posY1 = (float)Math.Round(position1.y, 1);
		float posY2 = (float)Math.Round(position2.y, 1);
		if (posX1 == posX2 && posY1 == posY2) {
			return true;
		}

		// Handle rounding problems
		/*if (posX1 < 0) posX1 = posX1 * -1;
		if (posX2 < 0) posX2 = posX2 * -1;
		if (posY1 < 0) posY1 = posY1 * -1;
		if (posY2 < 0) posY2 = posY2 * -1;
		float diff = (posX1 + posY1) - (posX2 + posY2);
		if (diff < 0) diff = diff * -1;*/

		float distance = LevelController.getDistanceBetweem(position1, position2);
		if (distance < 0) distance = distance * -1;
		if (distance < .01f) {
			return true;
		}

		return false;
	}

	public static bool isNotEqual(Vector3 position1, Vector3 position2) {
		return !isEqual(position1, position2);
	}

	public static void printDistanceBetween(Vector3 position1, Vector3 position2) {
		float distance = LevelController.getDistanceBetweem(position1, position2);
		Debug.Log("Distance: " + distance);
	}

	public static float getDistanceBetween(Vector3 position1, Vector3 position2) {
		return LevelController.getDistanceBetweem(position1, position2);
	}

	public static bool isOnSameLane(Vector3 position1, Vector3 position2) {
		float diff;
		diff = position1.x - position2.x;
		if (diff < 0) diff = diff * -1;
		if (diff < .1f) return true;

		diff = position1.y - position2.y;
		if (diff < 0) diff = diff * -1;		
		if (diff < .1f) return true;

		return false;
	}

}
