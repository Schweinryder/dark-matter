﻿using UnityEngine;
using System.Collections;

public class Tile {

	public Vector3 position { get; private set; }
	public int positionX; // { get; private set; }
	public int positionY; // { get; private set; }

	public Tile(Vector3 position, int xPos, int yPos) {
		this.position = position;
		this.positionX = xPos;
		this.positionY = yPos;
	}

}
