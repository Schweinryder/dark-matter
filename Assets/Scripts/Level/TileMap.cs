﻿using UnityEngine;
using System;
using System.Collections;

public class TileMap {

	Tile[,] tileMap;

	public void Init() {
		generateTileMap();
	}

	private void generateTileMap() {
		tileMap = new Tile[LevelController.LEVEL_SIZE_X, LevelController.LEVEL_SIZE_Y];
		float tileWidth = LevelController.gameAreaWidth() / (LevelController.LEVEL_SIZE_X -1);
		float tileHeight = LevelController.gameAreaHeight() / (LevelController.LEVEL_SIZE_Y -1);
		float leftBorder = LevelController.LEFT_BORDER;
		float bottomBorder = LevelController.BOTTOM_BORDER;

		for (int x = 0; x < LevelController.LEVEL_SIZE_X; x++) {
			for (int y = 0; y < LevelController.LEVEL_SIZE_Y; y++) {
				Vector3 tilePos = new Vector3(leftBorder + (tileWidth * x), bottomBorder + (tileHeight * y), 0);
				tileMap[x,y] = new Tile(tilePos, x, y);
			}
		}
	}

	public Tile[,] getTileMap() {
		return tileMap;
	}

	public Tile findClosestTileFor(Vector3 position) {
		Tile closestTile = null;
		float closestTileDistance = 1000f;
		for (int x = 0; x < LevelController.LEVEL_SIZE_X; x++) {
			for (int y = 0; y < LevelController.LEVEL_SIZE_Y; y++) {
				Tile currentTile = tileMap[x,y];
				float distanceToTile = getDistanceBetween(position, currentTile.position);
				if (distanceToTile < closestTileDistance) {
					closestTileDistance = distanceToTile;
					closestTile = currentTile;
				}
			}
		}
		return closestTile;
	}

	public float getDistanceBetween(Vector3 objectPosition, Vector3 tilePosition) {
		return (float) Math.Sqrt((Math.Pow(objectPosition.x - tilePosition.x, 2) + Math.Pow(objectPosition.y - tilePosition.y, 2)));
	}

}
