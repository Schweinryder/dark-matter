﻿using UnityEngine;
using System.Collections;

public class FragileWall : Wall {

	public AudioClip breakSound;

	public override void playerStuck(Character player) {
		LevelController.removeWall(this);
	}

	public override void pickupWall() {
		destory();
	}

	public override void destroyWall() {
		dropPowerUp();
		playSound();
		destory();
	}

	void playSound() {
		if (breakSound != null) {
			SoundController.playSound(breakSound);
		}
	}

	void dropPowerUp() {
		int value = Random.Range(0, 4);
		if (value > 0) { // 0 means no drop
			Vector3 powerUpPosition = new Vector3(transform.position.x, transform.position.y, Z.powerup);
			GameObject powerUpObject = Instantiate(GameController.powerUpObject, powerUpPosition, Quaternion.identity) as GameObject;
			PowerUp powerUp = powerUpObject.GetComponent<PowerUp> ();
			if (powerUp != null) {
				powerUp.init(value);
			}
		}
	}

}
