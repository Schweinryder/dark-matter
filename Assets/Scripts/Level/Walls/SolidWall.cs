﻿using UnityEngine;
using System.Collections;

public class SolidWall : Wall {

	public override void playerStuck(Character player) {

	}

	public override void pickupWall() {
		Debug.LogError("Never call pickupWall on a solid will, silly");
	}

	public override void destroyWall() {
		Debug.LogError("Never call destoryWall on a solid will, silly");
	}

}
