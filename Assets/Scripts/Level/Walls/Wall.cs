﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof(BoxCollider2D))]
public abstract class Wall : MonoBehaviour {
	
	void Start () {
		Tile tile = LevelController.getClosestTileFor(this.gameObject);
		transform.position = new Vector3(tile.position.x, tile.position.y, transform.position.z);
	}

	void OnTriggerEnter2D(Collider2D other) {
		Character player = other.gameObject.GetComponent<Character> ();
		if (player != null) {
			playerStuck(player);
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		Character player = other.gameObject.GetComponent<Character> ();
		if (player != null) {
			playerStuck(player);
		}
	}

	public abstract void playerStuck(Character player);

	public abstract void pickupWall();

	public abstract void destroyWall();

	protected void destory() {
		Destroy(this.gameObject);
	}

}
