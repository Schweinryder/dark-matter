﻿using UnityEngine;
using System;
using System.Collections;

public abstract class Movable : MonoBehaviour {

	public GameObject spriteObjectPrefab;

	protected Direction movingDirection;
	protected SpriteObject sprite;
	protected bool checkBorder = true, checkLane = true, checkPixel = true, setDirection = true;

	void LateUpdate() {
		if (checkBorder) {
			checkOutOfBounds();
		}
		if (checkLane) {
			placeOnMovableLane();
		}
		if (checkPixel) {
			placeOnPixelGrid();
		}
		if (setDirection) {
			setSpriteRotation(movingDirection);
		}
	}

	protected void checkOutOfBounds() {
		if (transform.position.x > LevelController.RIGHT_BORDER) {
			transform.position = setPositionX(LevelController.RIGHT_BORDER);
		}
		if (transform.position.x < LevelController.LEFT_BORDER) {
			transform.position = setPositionX(LevelController.LEFT_BORDER);
		}
		if (transform.position.y > LevelController.TOP_BORDER) {
			transform.position = setPositionY(LevelController.TOP_BORDER);
		}
		if (transform.position.y < LevelController.BOTTOM_BORDER) {
			transform.position = setPositionY(LevelController.BOTTOM_BORDER);
		}
	}

	protected void placeOnMovableLane() {
		Tile tile = LevelController.getClosestTileFor(this.gameObject);
		float xToX = getDistanceBetween(transform.position.x, tile.position.x);
		float yToY = getDistanceBetween(transform.position.y, tile.position.y);
		if (xToX < yToY) {
			transform.position = new Vector3(tile.position.x, transform.position.y, transform.position.z);
		} else {
			transform.position = new Vector3(transform.position.x, tile.position.y, transform.position.z);
		}
	}

	private float getDistanceBetween(float pointA, float pointB) {
		if (pointA < 0){
			pointA = pointA * -1;
		}
		if (pointB < 0) {
			pointB = pointB * -1;
		}
		float distanceBetween = pointA - pointB;
		if (distanceBetween < 0) {
			return distanceBetween * -1;
		}
		return distanceBetween;
	}

	private void placeOnPixelGrid() {
		/*float posX = LevelController.getPixelPositionX(transform.position.x);
		float posY = LevelController.getPixelPositionY(transform.position.y);

		//Debug.Log("closest position: " + posX + ":" + posY);
		Vector3 newPosition = new Vector3(posX, posY, transform.position.z);
		sprite.moveTo(newPosition);*/

		float x = (float)Math.Round(transform.position.x, 1);
		float y = (float)Math.Round(transform.position.y, 1);
		sprite.moveTo(new Vector3(x, y, Z.player));
	}

	Vector3 setPositionX(float posX) {
		return new Vector3(posX, transform.position.y, transform.position.z);
	}

	Vector3 setPositionY(float posY) {
		return new Vector3(transform.position.x, posY, transform.position.z);
	}

	public void moveTo(Vector3 newPosition) {
		transform.position = newPosition;
	}

	protected void setSpriteRotation(Direction direction) {
		sprite.setDirection(direction);
	}

	public void setSprite(Sprite newSprite) {
		sprite.setSprite(newSprite);
	}

}
