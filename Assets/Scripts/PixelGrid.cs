﻿using UnityEngine;
using System.Collections;

public class PixelGrid {

	float[] gridRows;
	float[] gridCols;

	public PixelGrid() {
		gridRows = new float[ScreenController.PX_WIDTH];
		gridCols = new float[ScreenController.PX_HEIGTH];

		float screenWidth = LevelController.gameAreaWidth();
		float startLeft = + LevelController.LEFT_BORDER;
		float pixelDistance = screenWidth / 54;
		for (int x = 0; x < ScreenController.PX_WIDTH; x++) {
			float posX = startLeft + (pixelDistance * x);
			gridRows[x] = posX;
		}

		float screenHeight = LevelController.gameAreaHeight();
		float startBottom = LevelController.BOTTOM_BORDER;
		pixelDistance = screenHeight / 54;
		for (int y = 0; y < ScreenController.PX_HEIGTH; y++) {
			gridCols[y] = startBottom + (pixelDistance * y);
		}
	}

	float getScreenHeigh() {
		return (ScreenController.SCREEN_PIXEL_BORDER_LEFT * -1) + ScreenController.SCREEN_PIXEL_BORDER_RIGHT;
	}

	float getScreenWidth() {
		return (ScreenController.SCREEN_PIXEL_BORDER_BOTTOM * -1) + ScreenController.SCREEN_PIXEL_BORDER_TOP;
	}

	public float getValidPositionX(float positionX) {
		float closestPosition = positionX;
		float distanceToClosest = 1000000000f;
		float currentPosition;
		float currentDistance;
		for (int x = 0; x < ScreenController.PX_WIDTH; x++) {
			currentPosition = gridCols[x];
			currentDistance = getDistanceBetween(positionX, currentPosition);
			if (currentDistance < distanceToClosest) {
				distanceToClosest = currentDistance;
				closestPosition = currentPosition;
			}
		}
		return closestPosition;
	}

	public float getValidPositionY(float positionY) {
		float closestPosition = positionY;
		float distanceToClosest = 1000000000f;
		float currentPosition;
		float currentDistance;
		for (int y = 0; y < ScreenController.PX_WIDTH; y++) {
			currentPosition = gridRows[y];
			currentDistance = getDistanceBetween(positionY, currentPosition);
			if (currentDistance < distanceToClosest) {
				distanceToClosest = currentDistance;
				closestPosition = currentPosition;
			}
		}
		return closestPosition;
	}

	private float getDistanceBetween(float pointA, float pointB) {
		if (pointA < 0){
			pointA = pointA * -1;
		}
		if (pointB < 0) {
			pointB = pointB * -1;
		}
		float distanceBetween = pointA - pointB;
		if (distanceBetween < 0) {
			return distanceBetween * -1;
		}
		return distanceBetween;
	}


}
