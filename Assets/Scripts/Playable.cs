﻿using UnityEngine;
using System.Collections;

public abstract class Playable : MonoBehaviour {

	protected int health;

	void Start() {
		Init();
	}

	protected abstract void Init();

	public bool isDead() {
		return health < 1;
	}

}
