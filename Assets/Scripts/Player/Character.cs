﻿using UnityEngine;
using System.Collections;

public abstract class Character : Movable {

	public AudioClip dropBombSound;

	private const float UNKILLABLE_TIMER = 2;

	public GameObject bombPrefab;
	public GameObject ghostPrefab;

	public int playerNumber {get; private set;}

	public int bombPower;
	public int bombLimit, wallLimit;
	public int wallsLeft;
	public float speed;

	protected Vector3 oldPosition;

	public int livesLeft { get; private set; }
	private float unkilladeCountdown;

	void Awake() {
		initSprite();
	}

	public virtual void Init(int playerNr) {
		this.playerNumber = playerNr;
		//	checkPixel = false;

		initGameValues();
	}

	private void initGameValues() {		
		movingDirection = Direction.Down;
		oldPosition = transform.position;
		livesLeft = 3;

		bombPower = 1;
		bombLimit = 1;
		wallsLeft = wallLimit = 0;

		speed = 2f;
	}

	private void initSprite() {
		GameObject expSpriteObj = Instantiate(spriteObjectPrefab, transform.position, Quaternion.identity) as GameObject;
		sprite = expSpriteObj.GetComponent<SpriteObject> ();
	}
	


	void FixedUpdate() {
		oldPosition = transform.position;

		updateMovement(speed);
		setMovingDirection();
	}

	protected abstract void updateMovement(float speed);

	void Update() {
		updateAction();
		if (unkilladeCountdown > 0) {
			unkilladeCountdown -= Time.deltaTime;
		}
	}

	protected abstract void updateAction();

	private void setMovingDirection() {
		if (transform.position.x > oldPosition.x) {
			movingDirection = Direction.Right;
			//Debug.Log("Moving right");
		} else if (transform.position.x < oldPosition.x) {
			movingDirection = Direction.Left;
			//Debug.Log("Moving left");
		} else if (transform.position.y > oldPosition.y) {
			movingDirection = Direction.Up;
			//Debug.Log("Moving up");
		} else if (transform.position.y < oldPosition.y) {
			movingDirection = Direction.Down;
			//Debug.Log("Moving Down");
		} 
	}

	public void pickUpPopwerUp(PowerUp powerUp) {
		if (powerUp.powerUpType == PowerUp.PowerUpType.HugerBombs) {
			if (bombPower < GameController.MaxBombPower) {				
				Debug.Log("Biggr 'splosions");
				bombPower++;
			}
		}
		if (powerUp.powerUpType == PowerUp.PowerUpType.SuperSpeed) {
			if (speed < GameController.MaxSpeed) {
				Debug.Log("Goin' fasta");
				speed += .5f;
			}
		}
		if (powerUp.powerUpType == PowerUp.PowerUpType.WallPickup) {
			if (wallLimit < GameController.MaxNumberOfWalls) {
				Debug.Log("Pick n place walls");
				wallLimit++;
			}
		}
		if (powerUp.powerUpType == PowerUp.PowerUpType.OneMoreBmb) {
			if (bombLimit < GameController.MaxNumberOfBombs) {
				Debug.Log("Anotha BOMBA");
				bombLimit++;
			}
		}
	}

	protected void placeWall() {
		if (canPlaceWall()) {
			LevelController.placeWallAheadOf(transform.position, movingDirection);
			wallsLeft--;
		}
	}

	protected void pickupWall() {
		if (canPickUpWall()) {
			bool pickedUp = LevelController.pickupWallAheadOf(transform.position, movingDirection);
			if (pickedUp) {
				wallsLeft++;
			} else {
				Debug.Log("Wall not picked up");
			}
		}
	}

	protected void placeBomb() {
		if (canPlaceBomb()) {
			Tile tile = LevelController.getClosestTileFor(this.gameObject);
			GameObject bombObject = (GameObject) Instantiate ( bombPrefab, new Vector3(tile.position.x, tile.position.y, Z.powerup), Quaternion.identity);
			Bomb bomb = bombObject.GetComponent<Bomb> ();
			if (bomb != null) {
				bomb.init(playerNumber, bombPower);
				playDropBombSound();
			} else {
				Debug.LogError("Bomb is null");
			}
		}
	}

	void playDropBombSound() {
		if (dropBombSound != null) {
			SoundController.playSound(dropBombSound);
		}
	}

	protected bool canPickUpWall() {
		return (wallLimit > 0) && (wallsLeft < wallLimit);
	}

	protected bool canPlaceWall() {
		return wallsLeft > 0;
	}

	protected bool canPlaceBomb() {
		return bombLimit > GameController.getNumberOfActiveBombForPlayer(playerNumber);
	}

	public virtual void applyDamage() {
		if (unkilladeCountdown <= 0) {
			livesLeft--;
			//Debug.Log("Life lost. Lives left: " + livesLeft);
			spawnGhost();
			if (livesLeft == 0) {
				GameController.iWasKilled(playerNumber);
				Destroy(sprite.gameObject);
				Destroy(this.gameObject);
			} else {
				unkilladeCountdown = UNKILLABLE_TIMER;
			}
		} else {
			//Debug.Log("No life lost, still unkillable");
		}
	}

	private void spawnGhost() {
		Instantiate (ghostPrefab, transform.position, Quaternion.identity);
	}

}
