using UnityEngine;
using System.Text;
using System.Collections;

public class Enemy : Character {

	EnemyAI ai;
	EnemyAI.Action action;

	float waitTimer;

	public override void Init(int playerNr) {
		base.Init(playerNr);
		setDirection = false;

		ai = new EnemyAI();
		waitTimer = 0;
	}

	// Called in FixedUpdate
	protected override void updateMovement(float speed) {
		if (ai.moving) {
			StringBuilder sb = new StringBuilder("Old: ");
			sb.Append(transform.position);
			transform.position = ai.getNextPosition(transform.position, speed);
			sb.Append(" - New: ");
			sb.Append(transform.position);
			//Debug.Log(sb.ToString());
		}
	}

	// Called in Update
	protected override void updateAction() {
		if (waiting()) return;

		action = ai.getNextAction(transform.position);
		if (action == EnemyAI.Action.Move) {
			setSpriteRotation(ai.currentPath.getDirection());
		}
		if (action == EnemyAI.Action.Bomb) {
			if (canPlaceBomb()) {
				placeBomb();
			}
		}
		if (action == EnemyAI.Action.Wait) {
			waitTimer = Random.Range(0f, 1.0f);
		}
	}

	private bool waiting() {
		if (waitTimer > 0) {
			waitTimer -= Time.deltaTime;
			return true;
		}
		return false;
	}

}
