﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAI {

	public enum Action {
		None,
		Wait,
		Move,
		Bomb
	}

	Direction lastDirection;
	Pathfinder pathfinder;
	public bool moving {get; private set;}
	public Path currentPath {get; private set;}
	Vector3 destination;

	bool holdBombs;

	// timers
	float waitTimer;

	public EnemyAI() {
		this.pathfinder = new Pathfinder();
		waitTimer = 2f; // Wait 2 sec
	}

	public Vector3 getNextPosition(Vector3 position, float speed) {
		Vector3 newPosition = position;
		Direction movementDirection = currentPath.getDirection(); 

		if (movementDirection == Direction.Up) { //if ( destination.y > position.y ) {
			//Debug.Log("Destination is above");
			newPosition = new Vector3(position.x, position.y + speed * Time.deltaTime, position.z);
		}
		if (movementDirection == Direction.Down) { //} else if ( destination.y < position.y ) {
			//Debug.Log("Destination is below");
			newPosition = new Vector3(position.x, position.y - speed * Time.deltaTime, position.z);
		}
		if (movementDirection == Direction.Right) { //} else if ( destination.x > position.x ) {
			//Debug.Log("Destination is to the right");
			newPosition = new Vector3(position.x + speed * Time.deltaTime, position.y, position.z);
		}
		if (movementDirection == Direction.Left) { //} else if ( destination.x < position.x ) {
			//Debug.Log("Destination is to the left");
			newPosition = new Vector3(position.x - speed * Time.deltaTime, position.y, position.z);
		}
		if (movementDirection == Direction.None) {
			Debug.LogWarning("There was no movement");
		}

		if (PositionUtils.isEqual(newPosition, destination)) {
			//Debug.Log("Destination reached");
			newPosition = LevelController.getClosestTileFor(newPosition).position;
			if (!currentPath.isEmpty()) {
				destination = currentPath.getNextPosition(true);
			} else {
				moving = false;
			}
		} else {
			//PositionUtils.printDistanceBetween(newPosition, destination);
		}

		if (newPosition == position) Debug.LogWarning("move() did not return a new position");
		return newPosition;
	}

	private Direction getMovementDirection(Vector3 currentPosition) {
		float xDiff = currentPosition.x - destination.x;
		float yDiff = currentPosition.y - destination.y;
		if (xDiff < 0) xDiff = xDiff * -1;
		if (yDiff < 0) yDiff = yDiff * -1;
		if (xDiff > yDiff) {
			if ( destination.x > currentPosition.x ) {
				return Direction.Right;
			} else {
				return Direction.Left;
			}
		} else if (yDiff > xDiff) {
			if ( destination.y > currentPosition.y) {
				return Direction.Up;
			} else {
				return Direction.Down;
			}
		} else return Direction.None;
	}

	public Action getNextAction(Vector3 position) {
		updateTimers();
		Action action = decideAction(position);
		return action;
	}

	private Action decideAction(Vector3 position) {
		if (timerIsTicking() || moving) return Action.None;
		float randomValue = Random.Range(0, 101);

		//bool canPlaceBomb = pathfinder.canPlaceBombAndMoveAway(position);
		//if (canPlaceBomb) Debug.Log("Can place bomb");
		//else Debug.Log("Can not place bomb");

		if (randomValue < 299) {
			//return Action.Wait;
		}


		if (randomValue <= 60) { // 60 % chance of movement
			bool isDangeroutPosition = pathfinder.isDangerousPosition(position);
			if (isDangeroutPosition) {
				Debug.LogWarning("This is a dangerous position");
				//return Action.Wait;
			} // else return Action.Nothing;
			//Debug.Log("Deciding new action");
			
			currentPath = getNextPath(position);
			if (!currentPath.isEmpty()) {
				if (!isDangeroutPosition && pathfinder.isDangerousPosition(currentPath.getNextPosition(false))) {
					currentPath = Path.emptyPath();
					return Action.Wait;
				}
				destination = currentPath.getNextPosition(true);
				moving = true;
				return Action.Move;
			}
		}

		if (randomValue <= 90) { // 30 % chance of placing bomb
			bool canPlaceBomb = pathfinder.canPlaceBombAndMoveAway(position);
			if (canPlaceBomb) return Action.Bomb;	
			else Debug.Log("Can not place bomb");
		}

		if (randomValue <= 100) { // 10 % chance of waiting
			//return Action.Wait;
		}

		return Action.None;
	}

	private Path getNextPath(Vector3 position) {
		List<Path> paths = pathfinder.getValidPathsFrom(position);
		if (paths.Count == 0) {
			Debug.LogWarning("At least one valid path should always be found");
			return Path.emptyPath();
		}
		lastDirection = getLastDirection();

		if (paths.Count > 1) {
			List<Path> safePaths = removeDangerousPaths(paths);
			if ((safePaths.Count > 0) && (Random.Range(0, 11) > 4)) { // 60% chance of using safe path
				paths = safePaths;
			}
		}
		if (paths.Count == 1) {
			return paths[0];
		}

		int randomIndex = Random.Range(0, paths.Count);
		Path pathInNewDirection = paths[randomIndex];
		Path pathInOldDirection = getPathWithDirection(paths, lastDirection);

		if (!pathInOldDirection.isEmpty()) {
			int randomInt = Random.Range(0, 101);

			if ( randomInt > 20 ) {
				return pathInOldDirection;
			} else {
				return pathInNewDirection;
			}
		}
		return pathInNewDirection;
	}

	private List<Path> removeDangerousPaths(List<Path> paths) {
		List<Path> safePaths = new List<Path>();
		foreach (Path path in paths) {
			if (!pathfinder.isDangerousPosition(path.getNextPosition(false))) {
				safePaths.Add(path);
			}
		}
		return safePaths;
	}

	Direction getLastDirection() {
		if (currentPath != null) {
			return currentPath.getDirection();
		} 
		return Direction.None;
	}

	Path getPathWithDirection(List<Path> paths, Direction lastDirection) {
		foreach (Path path in paths) {
			if (path.getDirection() == lastDirection) {
				return path;
			}
		}
		return Path.emptyPath();
	}

	void updateTimers() {
		if (waitTimer > 0) {
			waitTimer -= Time.deltaTime;
		}
	}

	bool timerIsTicking() {
		return waitTimer > 0;
	}

}
