using UnityEngine;
using System.Collections;

public class Player : Character {

	PlayerInputListener input;

	public override void Init(int playerNr) {
		base.Init(playerNr);
	}

	public void initInput(InputLayout layout) {		
		input = new PlayerInputListener(layout);
	}

	protected override void updateMovement(float speed) {
		transform.position = input.getNewPosition(transform.position, speed);
	}

	protected override void updateAction() {
		int action = input.getAction();
		if (action == 1) {
			placeBomb();
		}
		if (action == 2) {
			if (fragileWallIsAheadOf()) {
				pickupWall();
			} else {
				placeWall();
			}
		}
	}

	bool fragileWallIsAheadOf() {
		return false;
	}

}
