using UnityEngine;
using System.Collections;

public class PlayerInputListener {

	InputLayout layout;

	public PlayerInputListener(InputLayout layout) {
		this.layout = layout;
	}


	public Vector3 getNewPosition (Vector3 currentPosition, float speed) {
		Vector3 oldPosition = currentPosition;
		Vector3 newPosition = oldPosition;
		
		if ( Input.GetKey(layout.Up) ) {
			newPosition = new Vector3(newPosition.x, newPosition.y + speed * Time.deltaTime, newPosition.z);
		}
		if ( Input.GetKey(layout.Down) ) {
			newPosition = new Vector3(newPosition.x, newPosition.y - speed * Time.deltaTime, newPosition.z);
		}
		if ( Input.GetKey(layout.Right) ) {
			newPosition = new Vector3(newPosition.x + speed * Time.deltaTime, newPosition.y, newPosition.z);
		}
		if ( Input.GetKey(layout.Left) ) {
			newPosition = new Vector3(newPosition.x - speed * Time.deltaTime, newPosition.y, newPosition.z);
		}

		return newPosition;
	}

	public int getAction() {
		if ( Input.GetKeyDown(layout.bombAction) ) {
			return 1;
		}
		// TODO not used
		/*if ( Input.GetKeyDown(layout.wallAction) ) {
			return 2; 
		} */
		return 0;
	}

}
