﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public enum PowerUpType {
		None,
		SuperSpeed,
		HugerBombs,
		OneMoreBmb,
		WallPickup,
	}

	public AudioClip pickupSound;

	public Sprite hugerBombsSprite;
	public Sprite superSpeedSprite;
	public Sprite wallPickupSprite;
	public Sprite oneMoreBmbSprite;
	private SpriteObject sprite;

	public PowerUpType powerUpType {get; private set;}

	void Awake() {
		powerUpType = PowerUpType.None;
		sprite = GetComponentInChildren<SpriteObject> ();
	}

	public void init(int type) {
		if (type == 1) {
			powerUpType = PowerUpType.HugerBombs;
			sprite.setSprite(hugerBombsSprite);
		} else if (type == 2) {
			powerUpType = PowerUpType.SuperSpeed;
			sprite.setSprite(superSpeedSprite);
		} else if (type == 4) { // TODO not used right now
			powerUpType = PowerUpType.WallPickup;
			sprite.setSprite(wallPickupSprite);
		} else if (type == 3) {
			powerUpType = PowerUpType.OneMoreBmb;
			sprite.setSprite(oneMoreBmbSprite);
		} else {
			destroy();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Character player = other.gameObject.GetComponent<Character> ();
		if (player != null) {
			player.pickUpPopwerUp(this);
			playSound();
			destroy();
		}
	}

	void playSound() {
		if (pickupSound != null) {
			SoundController.playSound(pickupSound);
		}
	}

	void destroy() {
		Destroy(this.gameObject);
	}

}
