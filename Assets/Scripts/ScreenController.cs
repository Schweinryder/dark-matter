﻿using UnityEngine;
using System.Collections;

public class ScreenController : MonoBehaviour {

	public const float SCREEN_BORDER_FOR_SPRITE = 2.9f;
	public const float SCREEN_BORDER_FOR_PIXEL = 3.15f;

	public const float SCREEN_BORDER_TOP = 2.9f;
	public const float SCREEN_BORDER_BOTTOM = -2.9f;
	public const float SCREEN_BORDER_LEFT = -2.9f;
	public const float SCREEN_BORDER_RIGHT = 2.9f;

	public const float SCREEN_PIXEL_BORDER_TOP = 3.15f;
	public const float SCREEN_PIXEL_BORDER_BOTTOM = -3.15f;
	public const float SCREEN_PIXEL_BORDER_LEFT = -3.15f;
	public const float SCREEN_PIXEL_BORDER_RIGHT = 3.15f;

	public const int PX_WIDTH = 64;
	public const int PX_HEIGTH = 64;

	public static float Width;
	public static float Height;

	void Awake() {
		Screen.SetResolution(640, 640, false);
	}

}
