﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteRenderer))]
public class SpriteObject : MonoBehaviour {

	private SpriteRenderer spriteRenderer;

	private bool doFadeout;
	private float fadeoutTimer;

	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	void Update () {
		if (doFadeout) {
			StartCoroutine(fade ());
		}
	}

	IEnumerator fade() {
		float alpha = spriteRenderer.color.a;
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadeoutTimer) {
			Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, 0 ,t));
			spriteRenderer.color = newColor;
			yield return null;
		}
	}
	
	public void moveTo(Vector3 newPosition) {
		transform.position = newPosition;
	}

	public void setDirection(Direction direction) {
		if (direction == Direction.Up) {
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		if (direction == Direction.Right) {
			transform.rotation = Quaternion.Euler(0,0,270);
		}
		if (direction == Direction.Down) {
			transform.rotation = Quaternion.Euler(0,0,180);
		}
		if (direction == Direction.Left) {
			transform.rotation = Quaternion.Euler(0,0,90);
		}
	}

	public void startFadeout(float fadeoutTime) {
		doFadeout = true;
		this.fadeoutTimer = fadeoutTime;
	}

	public void setSprite(Sprite sprite) {
		if (spriteRenderer == null) {
			Debug.Log("FYI, sprite renderer in spriteobject is null when a method is called");
			spriteRenderer = GetComponent<SpriteRenderer> ();
		} else if (sprite == null) {
			Debug.Log("Sprite is friggin null");
		}
		spriteRenderer.sprite = sprite;
	}

}
