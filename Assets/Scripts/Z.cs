﻿using UnityEngine;
using System.Collections;

public static class Z {

	public const float invalid = 51;

	public const float level = 0;
	public const float powerup = -1;
	public const float player = -2;
	public const float wall = -3;
	public const float explosion = - 4;

}
