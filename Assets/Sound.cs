﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class Sound : MonoBehaviour {

	AudioSource soundPlayer;
	float timer;

	void Awake () {
		soundPlayer = GetComponent<AudioSource> ();
		timer = 1;
	}

	public void playSound(AudioClip sound) {
		soundPlayer.PlayOneShot(sound);
	}

	void Update() {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			Destroy(this.gameObject);
		}
	}

}
