﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

	public GameObject soundPrefab;
	static GameObject staticSoundObject;

	void Start() {
		staticSoundObject = soundPrefab;
	}
	
	public static void playSound(AudioClip soundToPlay) {
		GameObject soundObj = Instantiate(staticSoundObject, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		Sound sound = soundObj.GetComponent<Sound> ();
		sound.playSound(soundToPlay);
	}

}
