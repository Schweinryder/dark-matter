No collisions should be handled by code.
All movement of in-game objects should be done in FixedUpdate, this solved the bounciness issue when ridigbodies bounce off colliders (and going through them slightly).
